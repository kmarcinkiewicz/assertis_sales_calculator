Sales Calculator
================================================
simple application based on Cilex - Command Line Interface framework
see following diagram ./Php_application_Task.pdf

## Source files
```sh
./src/Cilex/Command/CalculateCommand.php
./src/Cilex/Utils/Date.php
```


## Usage

```sh
./bin/run.php calculate <path and filename where output csv file will be written>
./bin/run.php calculate ./example_output.csv
./bin/run.php calculate ~/example_output.csv
./bin/run.php calculate /home/user/example_output.csv
```
