<?php
/**
 * Created by PhpStorm.
 * User: Karol Marcinkiewicz
 * Date: 2017-05-18
 * Time: 06:31
 */

namespace Cilex\Utilis;

/**
 * Class Date
 * Utility class for data manipulation
 * @package Cilex\Utilis
 */
class Date
{
	/**
	 * @param int $month Number of month 1-12
	 *
	 * @return string Number of days in month of current year
	 */
	public static function days_in_month($month)
	{
		return date("t", strtotime(date("Y-{$month}-01")));
	}

	/**
	 * @param string $date Valid data in format "Y-m-d"
	 *
	 * @return string Day name of $date
	 */
	public static function day_name($date)
	{
		return date("l", strtotime(date($date)));
	}

	/**
	 * @param string $date Valid data in format "Y-m-d"
	 *
	 * @return string Return day name of calculated $date + 1 month
	 */
	public static function day_name_next_month($date)
	{
		return  date("l", strtotime("+1 months", strtotime(date($date))));
	}

	/**
	 * @param string $month_number
	 *
	 * @return string Month name
	 */
	public static function month_name($month_number)
	{
		return date('F', \mktime(0, 0, 0, $month_number,1));
	}



}