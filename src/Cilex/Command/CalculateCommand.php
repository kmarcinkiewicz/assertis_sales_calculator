<?php
/**
 * Created by PhpStorm.
 * User: Karol
 * Date: 2017-05-18
 * Time: 06:17
 */

namespace Cilex\Command;


use Cilex\Utilis\Date;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Cilex\Provider\Console\Command;

/**
 * Example command for testing purposes.
 */
class CalculateCommand extends Command
{
	protected $csv_array = array();
	protected $input;
	protected $output;

	/**
	 * {@inheritDoc}
	 */
	protected function configure()
	{
		$this
			->setName('calculate')
			->setDescription('Help utility to determine the dates to pay salaries')
			->addArgument('filename', InputArgument::REQUIRED, 'Output CSV filename');
	}

	/**
	 * {@inheritDoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->input = $input;
		$this->output = $output;
		$filename = $this->input->getArgument('filename');

		$this->checkValidParameter($filename);

		$this->csv_array['headers'] = array("Month name","Base Salary", "Bonus Salary");
		for($month = date("n"); $month<=12; $month++)
		{
			$this->csv_array[$month][]  = Date::month_name($month);
			$this->csv_array[$month][]  = $this->base_salary($month);
			$this->csv_array[$month][]  = $this->bonus_salary($month);
		}

		//$this->output->writeln(print_r($this->csv_array, true));

		$this->write_csv($filename);

	}

	/**
	 * @param int $month_number Processed month number at current year
	 *
	 * @return string Date of bonus salary. Every 15th day of month, bonus salary is paid unless that day is weekend,
	 * in that case they are paid the first Wednesday after 15th
	 */
	protected function bonus_salary($month_number)
	{
		for($i = 15; $i<=22; $i++)
		{
			$date_patern = "Y-{$month_number}-{$i}";
			$day_name_next_month = Date::day_name_next_month($date_patern);
			$processed_date = date("Y-m-d", strtotime("+1 months", strtotime(date($date_patern))));
			if(!in_array($day_name_next_month,array("Saturday", "Sunday")))
			{
				return $processed_date;
			}
			else
			{
				for($j=1;$j<=7;$j++)
				{
					$next_day_search = date("l", strtotime("+{$j} days",strtotime($processed_date)));
					if( $next_day_search == "Wednesday")
					{
						return date("Y-m-d", strtotime("+{$j} days",strtotime($processed_date)));
					}
				}
			}
		}
	}

	/**
	 * @param in $month_number Processed month number at current year
	 *
	 * @return string Date of base salaries. The base salaries are paid on the last day of the month, unless that day is weekday.
	 */
	protected function base_salary($month_number)
	{
		for($i = Date::days_in_month($month_number); $i>=0; $i--)
		{
			$date_pattern = "Y-{$month_number}-{$i}";
			$day_name = Date::day_name($date_pattern);
			if(!in_array($day_name,array("Saturday", "Sunday")))
			{
				return date("Y-m-d", strtotime(date($date_pattern)));
			}
		}
	}


	/**
	 * @param resource $filename Check if script has valid path and is writable
	 */
	protected function checkValidParameter($filename)
	{
		if(!\preg_match('/.+\.csv/', $filename, $matches) && \is_writable(dirname($filename)))
		{
			$this->output->writeln("Enter proper existing filename path, valid example path:");
			$this->output->writeln("   ./example.csv");
			$this->output->writeln("   ~/example.csv");
			$this->output->writeln("   /home/user/example.csv");
			$this->output->writeln("   /tmp/example.csv");
			exit;
		}
	}

	/**
	 * @param resource $filename Filename where output have to be written
	 */
	protected function write_csv($filename)
	{
		$fp = fopen($filename, 'w');

		foreach ($this->csv_array as $fields) {
			fputcsv($fp, $fields,";",'"');
		}

		fclose($fp);
		$this->output->writeln("Job done! CSV file {$filename} was saved succesfully.");
	}

}
